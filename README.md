
# Socket.IO Chat on REACT

## How to use

```
$ git clone https://motifej@bitbucket.org/motifej/react-chat.git
$ cd react-chat

$ npm install

## Start server socket.io at port 3000 
$ node server.js

## Start chatApp whith webpack at http://localhost:8080
$ npm start
```

And point your browser to `http://localhost:8080`. Optionally, specify
a port by supplying the `PORT` env variable.
