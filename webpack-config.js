var path = require("path");

var webpackConfig = {
    progress: true,
    failOnError: true,
    entry: {
        app: [
            './js/app.js'
        ]
    },
    output: {
        path: './build/',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                loader: "babel-loader",
                include: [
                    path.resolve(__dirname, "js"),
                ],
                test: /\.jsx?$/,
                query: {
                    presets: ['es2015', 'stage-0', 'react'],
                    plugins: ['transform-runtime', 'transform-decorators-legacy']
                }
            },
            { 
                test: /\.css$/,
                loaders: [
                    "style-loader",
                    'css-loader?sourceMap'
                ]
            }
        ]
    },
    devtool: 'source-map',
    devServer: {
        port: 8080,
        contentBase: path.join(__dirname, './'),
        stats: {
            colors: true
        },
        noInfo: false,
        quiet: false,
        hot: true
    }
};
module.exports = webpackConfig;
