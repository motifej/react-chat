import io from 'socket.io-client';
import dispatcher from '../dispatcher/ChatAppDispatcher'; 
import { appActions } from '../constant';
import { chatUrl } from '../config';


 var socket = io(chatUrl);

  var webApi = {
    send: function (action, mes) {
      socket.emit(action, mes);
    }
  };

  socket.on(appActions.LOGIN , function (data) {
    dispatcher.dispatch({type: appActions.LOGIN, data: data});
  })
  socket.on(appActions.NEW_MESSAGE, function (data) {
    dispatcher.dispatch({type: appActions.NEW_MESSAGE, data: data});
  })

  socket.on(appActions.USER_JOINED , function (data) {
    dispatcher.dispatch({type: appActions.USER_JOINED, data: data});
  })

  socket.on(appActions.USER_LEFT , function (data) {
    dispatcher.dispatch({type: appActions.USER_LEFT, data: data});
  })

  socket.on(appActions.TYPING , function (data) {
    dispatcher.dispatch({type: appActions.TYPING, data: data});
  })

  socket.on(appActions.STOP_TYPING , function (data) {
    dispatcher.dispatch({type: appActions.STOP_TYPING, data: data});
  })

  export default webApi;