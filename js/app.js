import React from 'react';
import ReactDOM from 'react-dom';
import '../css/chatapp.css';

import ChatApp from './components/ChatApp';

ReactDOM.render(
    <ChatApp />,
    document.getElementById('react')
);
