import dispatcher from '../dispatcher/ChatAppDispatcher'; 
import webApi from '../api/webApi';
import { appActions } from '../constant';

  var login;
  var messages = [];
  var userType = [];
  var listeners = {};


  var ChatStore = {
    connectWith: function (eventName, fn) {
      listeners[eventName] = fn;
    }
  };

  function addMessage(status, mess, text) {
    mess.status = status;
    if (text) {
      mess.text = text;
    }
    messages.push(mess);
    listeners['message'](messages);
    return messages.length - 1;
  };

  function checkPictInUrl(url, cb) {
    $('<img/>', {src: url})
      .load(function () {
        cb(url);
      });
  }

  function setPictInMessage(id, url) {
    var url = messages[id].message.match(/https?:\/\/[\S]+/g);
    if (url) {
      //checkPictInUrl(url[0], function (url) {
        messages[id].url = url;
        listeners['message'](messages);
      //})
    }
  }

  dispatcher.register(function (action) {
    var text;
    switch (action.type) {

      case appActions.LOGIN :
        text = 'Welcome to Socket.IO Chat \-';
        addMessage('log', action.data, text);
        break;

      case appActions.USER_JOINED :
        text = action.username + ' joined ' + new Date().toString().substr(8,16);
        addMessage('log', action.data, text);
        break;

      case appActions.USER_LEFT :
        text = action.username + ' left ' + new Date().toString().substr(8,16);
        addMessage('log', action.data, text);
        break;

      case appActions.ADD_USER :
        login = action.value;
        webApi.send(action.type, login);
        listeners['login'](login);
        break;

      case appActions.NEW_MESSAGE :
        if (!action.data.username) {
          webApi.send(action.type, action.data.message);
          action.data.username = login;
        }
        var id = addMessage('message', action.data);
        setPictInMessage(id);
        break;

      case appActions.TYPING :
        if(userType.indexOf(action.data.username)<0){
          userType.push(action.data.username);
          listeners['typing'](userType);
        }
        break;

      case appActions.STOP_TYPING :
        if(userType.indexOf(action.data.username)>=0) {
          userType.splice(userType.indexOf(action.data.username), 1);
          listeners['typing'](userType);
        }
        break;

      case appActions.I_TYPING :
        webApi.send(appActions.TYPING);
        break;

      case appActions.I_STOP_TYPING :
        webApi.send(appActions.STOP_TYPING);
        break;

      default:
      // do nothing
    }
  });

export default ChatStore;