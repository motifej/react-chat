var appActions = {
	LOGIN : 'login',
	USER_JOINED : 'user joined',
	USER_LEFT : 'user left',
	ADD_USER : 'add user',
	NEW_MESSAGE : 'new message',
	TYPING : 'typing',
	STOP_TYPING : 'stop typing',
	I_TYPING : 'i typing',
	I_STOP_TYPING : 'i stop typing'
}
export { appActions }