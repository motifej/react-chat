import React from 'react';
import dispatcher from '../dispatcher/ChatAppDispatcher';

export default class LoginPage extends React.Component {

  constructor() {
    super();
    this.state = {
      value : ''
    }
  }

  handleChange(event) {
    event.preventDefault();
    dispatcher.dispatch({type: "add user", value: event.target.childNodes[0].value});
  }

  render() {
    var value = this.state.value;
    return(
      <div className = 'form'>
        <h3 className="title">What's your nickname?</h3>
        <form onSubmit={this.handleChange}>
          <input className='usernameInput' type="text" maxLength='14' defaultValue=""/>
        </form>
      </div>
    )
  }
}