import React from 'react';
import ReactDOM from 'react-dom';
import ChatStore from '../store/ChatStore';
import dispatcher from '../dispatcher/ChatAppDispatcher'; 
import { debounce } from 'lodash';


export default class ChatPage extends React.Component {

  constructor() {
    super();
    this.state = {
      messages: [],
      userType : [],
      isTyping: false,
      colorOfUserName:{}
    }
  }

  componentWillMount() {
    ChatStore.connectWith('message', function(data){
      this.setState({messages: data});
    }.bind(this));
    ChatStore.connectWith('typing', function(users){
      this.setState({userType: users});
    }.bind(this));
  }

  componentWillUpdate() {
    var node = ReactDOM.findDOMNode(this).firstChild;
    this.shouldScrollBottom = node.scrollTop + node.offsetHeight === node.scrollHeight;
  }

  componentDidUpdate() {
    if (this.shouldScrollBottom) {
      var node = ReactDOM.findDOMNode(this).firstChild;
      node.scrollTop = node.scrollHeight
    }
  }

  _handleChange(event) {
    event.preventDefault();
    var input = event.target.childNodes[0];
    dispatcher.dispatch({type: "new message", data: {message:input.value}});
    input.value = '';
  }

  _setColorName(userName) {
    if(!this.state.colorOfUserName[userName])
      this.state.colorOfUserName[userName] = {color: 'rgb('
      + ~~(255*Math.random()) + ','
      + ~~(255*Math.random()) + ','
      + ~~(255*Math.random()) + ')'}
    return this.state.colorOfUserName[userName];
  }

  _onKeyDown() {
    this.state.isTyping = false;
    dispatcher.dispatch({type: "i stop typing"});
  }

  _onKeyUp(){
    if (!this.state.isTyping) {
      this.state.isTyping = true;
      dispatcher.dispatch({type: "i typing"});
    }
  }

    render() {
      var typing = '';
      if(this.state.userType.length){
        typing =
          (<li className='message typing'>
            {this.state.userType.map(function(name,i) {
              return <span className='username' style={this._setColorName(name)} key={i}>{name}</span>
            }.bind(this))}
            <span className='messageBody'>is typing</span>
           </li>)
      }
        return(
          <div className='chatArea'>
            <ul className='messages'>
                {this.state.messages.map(function(result,i) {
                  if (result.status === 'log')
                    return (
                      <li className='log' key={i}>
                        <span>{result.text}</span>
                        <br/>
                        <span>there are {result.numUsers} participants</span>
                      </li>
                    )
                  else {
                    var image ='';
                    if (result.url) {
                      image = <img src={result.url}/>
                    }
                    return (
                      <li className='message' key={i}>
                        <span className='username' style={this._setColorName(result.username)}>
                          {result.username}
                        </span>
                        <span className='messageBody'>{result.message}</span>
                        <br/>
                        {image}
                      </li>
                    )
                  }
                }.bind(this))}
              {typing}
            </ul>
            <form onSubmit={this._handleChange}
                  onKeyUp={::this._onKeyUp}
                  onKeyDown={debounce(::this._onKeyDown,500)}>
              <input className='inputMessage' type="text" placeholder='Type here...' defaultValue="" />
            </form>
         </div>)
    }

}

