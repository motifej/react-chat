import React from 'react';
import ChatPage from './ChatPage';
import LoginPage from './LoginPage';
import ChatStore from '../store/ChatStore';


export default class ChatApp extends React.Component {

  constructor() {
    super();
  }

  componentWillMount() {
    ChatStore.connectWith('login', function(login){
      this.setState({login: login});
    }.bind(this));
  }

  render() {
    if (!this.state) {
      return (
        <div className='login page'>
          <LoginPage/>
        </div>
      )
    } else {
      return (
        <div className='chat page'>
          <ChatPage/>
        </div>
      )
    }
  }
}